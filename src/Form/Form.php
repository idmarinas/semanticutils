<?php

/**
 * Semantic UI Utils
 *
 * @link      https://idmarinas@bitbucket.org/idmarinas/semanticutils.git for the canonical source repository
 * @copyright Copyright (c) 2017 Iván Diaz Marinas, IDMarinas. (http://www.infommo.es)
 * @license   See License.md
 */

namespace Idmarinas\SemanticUi\Form;

use Zend\Form\Form as ZendForm;

class Form extends ZendForm
{
    protected $securityElement = true;

    /**
     * Se reescribe el método isValid() del formulario para agregar la clase "error" a la etiqueta <form>
     */
    public function isValid()
    {
        $result = parent::isValid();
        if (! $result)
        {
            $this->setAttribute('class', $this->getAttribute('class') . ' error');
        }

        return $result;
    }

    /**
     * Configurar si se agrega o no el campo de seguridad
     * Default: true
     *
     * @param bool $security
     *
     * @return Idmarinas\SemanticUi\Form\Form
     */
    public function setSecurityElement(bool $security)
    {
        $this->securityElement = $security;

        return $this;
    }

    /**
     * Obtener si se agrega el campo de seguridad
     *
     * @return bool
     */
    public function getSecurityElement()
    {
        return $this->securityElement;
    }
}
