<?php

/**
 * Semantic UI Utils
 *
 * @link      https://idmarinas@bitbucket.org/idmarinas/semanticutils for the canonical source repository
 * @copyright Copyright (c) 2017 Iván Diaz Marinas, IDMarinas. (http://www.infommo.es)
 * @license   See License.md
 */

namespace Idmarinas\SemanticUi;

class ConfigProvider
{
    /**
     * Return general configuration.
     *
     * @return array
     */
    public function __invoke()
    {
        return [
			'dependencies' => $this->getDependencyConfig()
        ];
    }

    /**
	 * Obtener la configuración
	 *
	 * @return array
	 */
    public function getDependencyConfig()
    {
        return [];
    }
}
