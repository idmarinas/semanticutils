<?php

/**
 * Semantic UI Utils
 *
 * @link      https://idmarinas@bitbucket.org/idmarinas/semanticutils for the canonical source repository
 * @copyright Copyright (c) 2017 Iván Diaz Marinas, IDMarinas. (http://www.infommo.es)
 * @license   See License.md
 */

namespace Idmarinas\SemanticUi\View\Helper;

use Idmarinas\SemanticUi\Pattern;

use Zend\Form\ElementInterface;
use Zend\Form\Exception;
use Zend\Form\LabelAwareInterface;

use Zend\Form\View\Helper\FormLabel as ZendLabel;

class FormLabel extends ZendLabel
{
	use Pattern\PrepareAttributes;

    /**
     * Generate an opening label tag
     *
     * @param  null|array|ElementInterface $attributesOrElement
     * @throws Exception\InvalidArgumentException
     * @throws Exception\DomainException
     * @return string
     */
    public function openTag($attributesOrElement = null)
    {
        if (null === $attributesOrElement)
        {
            return '<div class="field">';
        }

        if (is_array($attributesOrElement))
        {
            if (isset($attributesOrElement['class']) && false === strpos($attributesOrElement['class'], 'field'))
            {
                $attributesOrElement['class'] = $attributesOrElement['class'] . ' field';
            }

            $attributes = $this->createAttributesString($attributesOrElement);
            return sprintf('<div %s>', $attributes);
        }

        if (! $attributesOrElement instanceof ElementInterface)
        {
            throw new Exception\InvalidArgumentException(sprintf(
                '%s expects an array or Zend\Form\ElementInterface instance; received "%s" %s',
                __METHOD__,
                (is_object($attributesOrElement) ? get_class($attributesOrElement) : gettype($attributesOrElement)),
                $attributesOrElement->getLabel()
            ));
        }

        $id = $this->getId($attributesOrElement);
        if (null === $id) {
            throw new Exception\DomainException(sprintf(
                '%s expects the Element provided to have either a name or an id present; neither found',
                __METHOD__
            ));
        }

        $labelAttributes = [];
        if ($attributesOrElement instanceof LabelAwareInterface)
        {
            $labelAttributes = $attributesOrElement->getLabelAttributes();
        }

        $attributes = ['for' => $id];

        if (! empty($labelAttributes))
        {
            $attributes = array_merge($labelAttributes, $attributes);
        }

        $attributes = $this->createAttributesString($attributes);

        return sprintf('<div %s>', $attributes);
    }

    /**
     * Return a closing label tag
     *
     * @return string
     */
    public function closeTag()
    {
        return '</div>';
    }
}
