<?php

/**
 * Semantic UI Utils
 *
 * @link      https://idmarinas@bitbucket.org/idmarinas/semanticutils for the canonical source repository
 * @copyright Copyright (c) 2017 Iván Diaz Marinas, IDMarinas. (http://www.infommo.es)
 * @license   See License.md
 */

namespace Idmarinas\SemanticUi\View\Helper;

use Zend\Form\ElementInterface;
use Zend\Form\Element\Button;
use Zend\Form\Element\Captcha;
use Zend\Form\Element\Collection as CollectionElement;
use Zend\Form\FieldsetInterface;
use Zend\Form\LabelAwareInterface;
use Zend\Form\View\Helper\FormCollection as ZendFormCollection;


class FormCollection extends ZendFormCollection
{
    /**
     * This is the default wrapper that the collection is wrapped into
     *
     * @var string
     */
    protected $wrapper = '%2$s<div%4$s>%1$s%3$s</div>';

    /**
     * This is the default label-wrapper
     *
     * @var string
     */
    protected $labelWrapper = '<h3 class="ui attached header">%s</h3>';

    /**
     * Render a collection by iterating through all fieldsets and elements
     *
     * @param  ElementInterface $element
     * @return string
     */
    public function render(ElementInterface $element)
    {
        $renderer = $this->getView();
        if (! method_exists($renderer, 'plugin'))
        {
            // Bail early if renderer is not pluggable
            return '';
        }

        $markup = '';
        $templateMarkup = '';
        $elementHelper = $this->getElementHelper();
        $fieldsetHelper = $this->getFieldsetHelper();

        if ($element instanceof CollectionElement && $element->shouldCreateTemplate())
        {
            $templateMarkup = $this->renderTemplate($element);
        }

        foreach ($element->getIterator() as $elementOrFieldset)
        {
            if ($elementOrFieldset instanceof FieldsetInterface)
            {
               $markup .= $fieldsetHelper($elementOrFieldset, $this->shouldWrap());
            }
            else if ($elementOrFieldset instanceof ElementInterface)
            {
                $markup .= $elementHelper($elementOrFieldset);
            }
        }

        // Every collection is wrapped by a fieldset if needed
        if ($this->shouldWrap)
        {
            $attributes = $element->getAttributes();
            //-- Agregar la clase segmento y la colección no es el formulario principal
            if (isset($attributes['class']))
            {
                if (false !== strpos($attributes['class'], 'ui form'))
                {
                    $attributes['class'] = 'ui attached segment';
                }
            }
            unset($attributes['name']);
            $attributesString = count($attributes) ? ' ' . $this->createAttributesString($attributes) : '';

            $label = $element->getLabel();
            $legend = '';

            if (! empty($label))
            {
                if (null !== ($translator = $this->getTranslator()))
                {
                    $label = $translator->translate(
                        $label,
                        $this->getTranslatorTextDomain()
                    );
                }

                if (! $element instanceof LabelAwareInterface || ! $element->getLabelOption('disable_html_escape'))
                {
                    $escapeHtmlHelper = $this->getEscapeHtmlHelper();
                    $label = $escapeHtmlHelper($label);
                }

                $legend = sprintf(
                    $this->labelWrapper,
                    $label
                );
            }

            $markup = sprintf(
                $this->wrapper,
                $markup,
                $legend,
                $templateMarkup,
                $attributesString
            );
        }
        else
        {
            $markup .= $templateMarkup;
        }

        return $markup;
    }
}
