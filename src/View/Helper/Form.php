<?php

/**
 * Semantic UI Utils
 *
 * @link      https://idmarinas@bitbucket.org/idmarinas/semanticutils for the canonical source repository
 * @copyright Copyright (c) 2017 Iván Diaz Marinas, IDMarinas. (http://www.infommo.es)
 * @license   See License.md
 */

namespace Idmarinas\SemanticUi\View\Helper;

use Idmarinas\SemanticUi\Pattern;

use Zend\Form\View\Helper\Form as ZendForm;
use Zend\Form\FieldsetInterface;
use Zend\Form\FormInterface;
use Zend\Form\LabelAwareInterface;

class Form extends ZendForm
{
	use Pattern\PrepareAttributes;

	/**
	 * Render a form from the provided $form,
	 *
	 * @param  FormInterface $form
	 * @return string
	 */
	public function render(FormInterface $form)
	{
		if (method_exists($form, 'prepare')) $form->prepare();

		$formContent = '';
		$formElements = '';

		//-- Label para el formulario si lo tiene
		$label = $form->getLabel();
		if (! empty($label))
		{
			if (null !== ($translator = $this->getTranslator()))
			{
				$label = $translator->translate(
					$label,
					$this->getTranslatorTextDomain()
				);
			}

			if (! $form instanceof LabelAwareInterface || ! $form->getLabelOption('disable_html_escape'))
			{
				$escapeHtmlHelper = $this->getEscapeHtmlHelper();
				$label = $escapeHtmlHelper($label);
			}

			$formContent .= sprintf(
				'<h3 class="ui top attached header">%s</h3>',
				$label
			);
        }

        //-- Agregar el campo de seguridad
        if ($form->getSecurityElement())
        {
            $formContent .= $this->getView()->formRow(new \Zend\Form\Element\Csrf('security'));
        }

		foreach ($form as $element)
		{
			if ($element instanceof FieldsetInterface)
			{
				if (! empty($formElements))
                {
                    $formContent .= '<div class="ui attached segment">'.$formElements.'</div>';
                    $formElements = '';
                }

				$formContent .= $this->getView()->formCollection($element);
			}
			else
			{
				$formElements .= $this->getView()->formRow($element);
			}
		}

		if (! empty($formElements))  $formContent .= '<div class="ui attached segment">'.$formElements.'</div>';

		return $this->openTag($form) . $formContent . $this->closeTag();
	}
}
