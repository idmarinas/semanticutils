<?php

/**
 * Semantic UI Utils
 *
 * @link      https://idmarinas@bitbucket.org/idmarinas/semanticutils for the canonical source repository
 * @copyright Copyright (c) 2017 Iván Diaz Marinas, IDMarinas. (http://www.infommo.es)
 * @license   See License.md
 */

namespace Idmarinas\SemanticUi\View\Helper;

use Zend\Form\ElementInterface;
use Zend\Form\Element\Button;
use Zend\Form\Element\MonthSelect;
use Zend\Form\LabelAwareInterface;
use Zend\Form\View\Helper\FormRow as ZendFormRow;

class FormRow extends ZendFormRow
{
    /**
     * The class that is added to element that have errors
     *
     * @var string
     */
    protected $inputErrorClass = 'error';

    /**
     * Utility form helper that renders a label (if it exists), an element and errors
     *
     * @param  ElementInterface $element
     * @param  null|string      $labelPosition
     * @throws \Zend\Form\Exception\DomainException
     * @return string
     */
    public function render(ElementInterface $element, $labelPosition = NULL)
    {
        $escapeHtmlHelper = $this->getEscapeHtmlHelper();
        $labelHelper = $this->getLabelHelper();
        $elementHelper = $this->getElementHelper();

        $elementErrorsHelper = $this->getElementErrorsHelper();
        $elementErrorsHelper->setMessageOpenFormat('<div class="ui small error message"><div class="ui divided list"><div class="item">')
            ->setMessageSeparatorString('</div><div class="item">')
            ->setMessageCloseString('</div></div></div>');

        $label = $element->getLabel();
        $inputErrorClass = $this->getInputErrorClass();

        if (is_null($labelPosition))
        {
            $labelPosition = $this->labelPosition;
        }

        if (isset($label) && '' !== $label)
        {
            // Translate the label
            if (null !== ($translator = $this->getTranslator()))
            {
                $label = $translator->translate($label, $this->getTranslatorTextDomain());
            }
        }

        // Does this element have errors ?
        if (count($element->getMessages()) > 0 && ! empty($inputErrorClass))
        {
            $classAttributes = ($element->hasAttribute('class') ? $element->getAttribute('class') . ' ' : '');
            $classAttributes = $classAttributes . $inputErrorClass;

            $element->setAttribute('class', $classAttributes);
        }

        if ($this->partial)
        {
            $vars = [
                'element' => $element,
                'label' => $label,
                'labelAttributes' => $this->labelAttributes,
                'labelPosition' => $labelPosition,
                'renderErrors' => $this->renderErrors,
            ];

            return $this->view->render($this->partial, $vars);
        }

        if ($this->renderErrors)
        {
            $elementErrors = $elementErrorsHelper->render($element);
        }

        $elementString = $elementHelper->render($element);

        //-- Agregar el estilo para Semantic UI
        $semantic = $element->getOption('semantic');
        if ($semantic)
        {
            if (in_array($semantic['type'], ['input']))
            {
                $class = 'Idmarinas\\SemanticUi\\View\\Helper\\Semantic\\' . ucfirst($semantic['type']);

                $semantic = new $class($semantic, $elementString);
                $elementString = $semantic->render();
            }
        }
        unset($semantic);

        // hidden elements do not need a <label>
        $type = $element->getAttribute('type');
        if (isset($label) && '' !== $label && $type !== 'hidden')
        {
            $labelAttributes = [];

            if ($element instanceof LabelAwareInterface)
            {
                $labelAttributes = $element->getLabelAttributes();
            }

            if (! $element instanceof LabelAwareInterface || ! $element->getLabelOption('disable_html_escape'))
            {
                $label = $escapeHtmlHelper($label);
            }

            if (empty($labelAttributes))
            {
                $labelAttributes = $this->labelAttributes;
            }

            if (! empty($elementErrors))
            {
                if (isset($labelAttributes['class']) && ! empty($labelAttributes['class'])) $labelAttributes['class'] = $labelAttributes['class'] . ' error';
                else  $labelAttributes['class'] = 'error';
            }

            if ($element->getAttribute('required') == true)
            {
                if (isset($labelAttributes['class']) && ! empty($labelAttributes['class'])) $labelAttributes['class'] = 'required ' . $labelAttributes['class'];
                else  $labelAttributes['class'] = 'required';
            }

            // Multicheckbox elements have to be handled differently as the HTML standard does not allow nested
            // labels. The semantic way is to group them inside a fieldset
            if ($type === 'multi_checkbox'
                || $type === 'radio'
                || $element instanceof MonthSelect
            ) {
                $markup = sprintf(
                    '<fieldset><legend>%s</legend>%s</fieldset>',
                    $label,
                    $elementString
                );
            } else {
                // Ensure element and label will be separated if element has an `id`-attribute.
                // If element has label option `always_wrap` it will be nested in any case.
                if ($element->hasAttribute('id')
                    && ($element instanceof LabelAwareInterface && ! $element->getLabelOption('always_wrap'))
                ) {
                    $labelOpen = '';
                    $labelClose = '';
                    $label = $labelHelper->openTag($element) . $label . $labelHelper->closeTag();
                } else {
                    $labelOpen  = $labelHelper->openTag($labelAttributes);
                    $labelClose = $labelHelper->closeTag();
                }

                if ($label !== '' && (! $element->hasAttribute('id'))
                    || ($element instanceof LabelAwareInterface && $element->getLabelOption('always_wrap'))
                ) {
                    $label = '<label>' . $label . '</label>';
                }

                // Button element is a special case, because label is always rendered inside it
                if ($element instanceof Button)
                {
                    $labelOpen = $labelClose = $label = '';
                }

                if ($element instanceof LabelAwareInterface && $element->getLabelOption('label_position')) {
                    $labelPosition = $element->getLabelOption('label_position');
                }

                switch ($labelPosition)
                {
                    case self::LABEL_PREPEND:
                        $markup = $labelOpen . $label . $elementString . $labelClose;
                        break;
                    case self::LABEL_APPEND:
                    default:
                        $markup = $labelOpen . $elementString . $label . $labelClose;
                        break;
                }
            }

            if ($this->renderErrors)
            {
                $markup .= $elementErrors;
            }
        }
        else
        {
            if ($this->renderErrors) { $markup = $elementString . $elementErrors; }
        }

        return $markup;
    }

    /**
     * Retrieve the FormElement helper
     *
     * @return FormElement
     */
    protected function getElementHelper()
    {
        if ($this->elementHelper) {
            return $this->elementHelper;
        }

        if (method_exists($this->view, 'plugin')) {
            $this->elementHelper = $this->view->plugin('form_element');
        }

        if (! $this->elementHelper instanceof FormElement) {
            $this->elementHelper = new FormElement();
            $this->elementHelper->setView($this->view);
        }

        return $this->elementHelper;
    }
}
