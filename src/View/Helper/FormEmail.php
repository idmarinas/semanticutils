<?php

/**
 * Semantic UI Utils
 *
 * @link      https://idmarinas@bitbucket.org/idmarinas/semanticutils for the canonical source repository
 * @copyright Copyright (c) 2017 Iván Diaz Marinas, IDMarinas. (http://www.infommo.es)
 * @license   See License.md
 */

namespace Idmarinas\SemanticUi\View\Helper;

use Idmarinas\SemanticUi\Pattern;

use Zend\Form\View\Helper\FormEmail as ZendEmail;

class FormEmail extends ZendEmail
{
	use Pattern\PrepareAttributes;
}
