<?php

/**
 * Semantic UI Utils
 *
 * @link      https://idmarinas@bitbucket.org/idmarinas/semanticutils for the canonical source repository
 * @copyright Copyright (c) 2017 Iván Diaz Marinas, IDMarinas. (http://www.infommo.es)
 * @license   See License.md
 */

namespace Idmarinas\SemanticUi\View\Helper\Captcha;

use Idmarinas\SemanticUi\Pattern;

use Zend\Captcha\AdapterInterface as CaptchaAdapter;
use Zend\Form\ElementInterface;
use Zend\Form\Exception;
use Zend\Form\View\Helper\Captcha\Image as ZendImage;

class Image extends ZendImage
{
	use Pattern\PrepareAttributes;

	/**
     * Render the hidden input with the captcha identifier
     *
     * @param  CaptchaAdapter $captcha
     * @param  array          $attributes
     * @return string
     */
    protected function renderCaptchaHidden(CaptchaAdapter $captcha, array $attributes)
    {
        $attributes['type']  = 'hidden';
        $attributes['name'] .= '[id]';

        if (isset($attributes['id'])) {
            $attributes['id'] .= '-hidden';
        }

        if (method_exists($captcha, 'getId')) {
            $attributes['value'] = $captcha->getId();
        } elseif (array_key_exists('value', $attributes)) {
            if (is_array($attributes['value']) && array_key_exists('id', $attributes['value'])) {
                $attributes['value'] = $attributes['value']['id'];
            }
        }

		//-- Se eliminan los atributos propios de VueJS
		foreach($attributes as $key => $value)
		{
			if ('v-' == substr($key, 0, 2)) unset($attributes[$key]);
		}

        $closingBracket      = $this->getInlineClosingBracket();
        $hidden              = sprintf(
            '<input %s%s',
            $this->createAttributesString($attributes),
            $closingBracket
        );

        return $hidden;
    }
}
