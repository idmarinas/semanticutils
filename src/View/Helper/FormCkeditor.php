<?php

/**
 * Semantic UI Utils
 *
 * @link      https://idmarinas@bitbucket.org/idmarinas/semanticutils for the canonical source repository
 * @copyright Copyright (c) 2017 Iván Diaz Marinas, IDMarinas. (http://www.infommo.es)
 * @license   See License.md
 */

namespace Idmarinas\SemanticUi\View\Helper;

use Idmarinas\SemanticUi\Pattern;

use Zend\Form\ElementInterface;

use Zend\Form\View\Helper\AbstractHelper;

class FormCkeditor extends AbstractHelper
{
    use Pattern\PrepareAttributes;

    /**
     * Attributes valid for the input tag
     *
     * @var array
     */
    protected $validTagAttributes = [
        'autocomplete' => true,
        'autofocus' => true,
        'cols' => true,
        'dirname' => true,
        'disabled' => true,
        'form' => true,
        'maxlength' => true,
        'name' => true,
        'placeholder' => true,
        'readonly' => true,
        'required' => true,
        'rows' => true,
        'wrap' => true,
        'type-editor' => true,
        'content' => true
    ];

    /**
     * Invoke helper as functor
     *
     * Proxies to {@link render()}.
     *
     * @param  ElementInterface|null $element
     * @return string|FormTextarea
     */
    public function __invoke(ElementInterface $element = null)
    {
        if (! $element) { return $this; }

        return $this->render($element);
    }

    /**
     * Render a form <textarea> element from the provided $element
     *
     * @param  ElementInterface $element
     * @throws Exception\DomainException
     * @return string
     */
    public function render(ElementInterface $element)
    {
        $name = $element->getName();
        if (empty($name) && $name !== 0)
        {
            throw new Exception\DomainException(sprintf(
                '%s requires that the element has an assigned name; none discovered',
                __METHOD__
            ));
        }

        $attributes = $element->getAttributes();
        $attributes['name'] = $name;
        $content = (string) $element->getValue();
        $escapeHtml = $this->getEscapeHtmlHelper();

        return sprintf(
            '<ckeditor5 %s content="%s"></ckeditor5>',
            $this->createAttributesString($attributes),
            $escapeHtml($content)
        );
    }
}
