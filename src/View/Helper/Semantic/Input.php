<?php

/**
 * Semantic UI Utils
 *
 * @link      https://idmarinas@bitbucket.org/idmarinas/semanticutils for the canonical source repository
 * @copyright Copyright (c) 2017 Iván Diaz Marinas, IDMarinas. (http://www.infommo.es)
 * @license   See License.md
 */

namespace Idmarinas\SemanticUi\View\Helper\Semantic;

use Idmarinas\SemanticUi\Pattern\PrepareAttributes;

use Zend\Form\View\Helper\AbstractHelper;

class Input extends AbstractHelper
{
	use PrepareAttributes;

    /**
     * Es el elemento al que se va a envolver
     *
     * @var string
     */
    protected $elementString;

    /**
     * Este es el envoltorio por defecto del input
     *
     * @var string
     */
    protected $wrapper = '<div class="ui %1$s input">%2$s %3$s %4$s %5$s</div>';

    /**
     * Clase adicional para el input
     *
     * @var string
     */
    protected $classWrapper = '';

    /**
     * Elementos que se van a incorporar al input
     */
    protected $elements = [];

    /**
     * Se construye
     */
    public function __construct(array $options, $elementString)
    {
        //-- Elementos que se incorporan al input
        if (isset($options['elements'])) $this->elements = $options['elements'];

        //-- Clase adicional para el input
        if (isset($options['class'])) $this->classWrapper = $options['class'];

        if (is_string($elementString)) $this->elementString = $elementString;
    }

    /**
     * Se genera el nuevo elemenString
     *
     * @return string
     */
    public function render()
    {
        $icon = '';
        $label = '';
        $action = '';
        //-- Cambiar la posición de los input - label a la derecha y action a la izquierda
        if (false !== strpos($this->classWrapper, 'right labeled') && false !== strpos($this->classWrapper, 'left action'))
        {
            $this->wrapper = '<div class="ui %1$s input">%2$s %5$s %4$s %3$s</div>';
        }
        //-- Cambiar la posición sólo del action
        else if (false !== strpos($this->classWrapper, 'left action'))
        {
            $this->wrapper = '<div class="ui %1$s input">%5$s %2$s %4$s</div>';
        }
        //-- Cambiar la posición solo del label
        else if (false !== strpos($this->classWrapper, 'right labeled'))
        {
            $this->wrapper = '<div class="ui %1$s input">%3$s %2$s %4$s</div>';
        }

        foreach($this->elements as $element)
        {
            if ('icon' == $element['type'])
            {
                $icon .= sprintf('<i class="%s"></i>', $element['icon']);
            }
            else if ('action' == $element['type'])
            {
                $action .= sprintf('<button type="button" %s>%s</button>', $this->createAttributesString($element['attributes']), $element['label']);
            }
            else if ('label' == $element['type'])
            {
                $label .= sprintf('<div %s>%s</div>', $this->createAttributesString($element['attributes']), $element['label']);
            }
        }

        $string = '<div class="ui %1$s input">%2$s %3$s %4$s %5$s</div>';

        return sprintf($this->wrapper,
            $this->classWrapper, // 1
            $icon, // 2
            $label, // 3
            $this->elementString, // 4
            $action // 5
        );
    }
}
