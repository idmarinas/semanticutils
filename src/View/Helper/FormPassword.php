<?php

/**
 * Semantic UI Utils
 *
 * @link      https://idmarinas@bitbucket.org/idmarinas/semanticutils for the canonical source repository
 * @copyright Copyright (c) 2017 Iván Diaz Marinas, IDMarinas. (http://www.infommo.es)
 * @license   See License.md
 */

namespace Idmarinas\SemanticUi\View\Helper;

use Idmarinas\SemanticUi\Pattern;

use Zend\Form\ElementInterface;
use Zend\Form\View\Helper\FormPassword as ZendPassword;

class FormPassword extends ZendPassword
{
	use Pattern\PrepareAttributes;

	/**
     * Render a form <input> element from the provided $element
     *
     * @param  ElementInterface $element
     * @throws Exception\DomainException
     * @return string
     */
    public function render(ElementInterface $element)
    {
        $name = $element->getName();
        if ($name === null || $name === '') {
            throw new Exception\DomainException(sprintf(
                '%s requires that the element has an assigned name; none discovered',
                __METHOD__
            ));
        }

        $attributes          = $element->getAttributes();
        $attributes['name']  = $name;
        $type                = $this->getType($element);
        $attributes['type']  = $type;
        $attributes['value'] = $element->getValue();
        if ('password' == $type) {
            $attributes['value'] = '';
        }

		if (! isset($attributes['password-strength']))
		{
			return sprintf(
				'<input %s%s',
				$this->createAttributesString($attributes),
				$this->getInlineClosingBracket()
			);
		}
		else
		{
			return sprintf(
				'<password %s></password>',
				$this->createAttributesString($attributes)
			);
		}
    }
}
