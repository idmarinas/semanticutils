<?php

/**
 * Semantic UI Utils
 *
 * @link      https://idmarinas@bitbucket.org/idmarinas/semanticutils for the canonical source repository
 * @copyright Copyright (c) 2017 Iván Diaz Marinas, IDMarinas. (http://www.infommo.es)
 * @license   See License.md
 */

namespace Idmarinas\SemanticUi\View\Navigation;

use RecursiveIteratorIterator;

use Zend\View\Helper\Navigation\Menu as ZendMenu;

use Zend\Navigation\AbstractContainer;
use Zend\Navigation\Page\AbstractPage;

/**
 * Helper for rendering menus from navigation containers
 */
class SemanticMenu extends ZendMenu
{
	/**
	 * CSS class for style menu
	 * @url http://semantic-ui.com/collections/menu.html
	 *
	 * @var string
	 */
	protected $ulClass = 'ui attached pointing menu master';

		/**
	 * Renders a normal menu (called from {@link renderMenu()}).
	 *
	 * @param  AbstractContainer $container          container to render
	 * @param  string            $ulClass            CSS class for first UL
	 * @param  string            $indent             initial indentation
	 * @param  int|null          $minDepth           minimum depth
	 * @param  int|null          $maxDepth           maximum depth
	 * @param  bool              $onlyActive         render only active branch?
	 * @param  bool              $escapeLabels       Whether or not to escape the labels
	 * @param  bool              $addClassToListItem Whether or not page class applied to <li> element
	 * @param  string            $liActiveClass      CSS class for active LI
	 * @return string
	 */
	protected function renderNormalMenu(
		AbstractContainer $container,
		$ulClass,
		$indent,
		$minDepth,
		$maxDepth,
		$onlyActive,
		$escapeLabels,
		$addClassToListItem,
		$liActiveClass
	) {
		$addClassToListItem = false;
		$html = '';

		// find deepest active
		$found = $this->findActive($container, $minDepth, $maxDepth);

		/* @var $escaper \Zend\View\Helper\EscapeHtmlAttr */
		$escaper = $this->view->plugin('escapeHtmlAttr');

		if ($found)
        {
			$foundPage  = $found['page'];
			$foundDepth = $found['depth'];
		}
        else
        {
			$foundPage = null;
		}

		// create iterator
		$iterator = new RecursiveIteratorIterator($container, RecursiveIteratorIterator::SELF_FIRST);

		if (is_int($maxDepth)) $iterator->setMaxDepth($maxDepth);

		// iterate container
		$prevDepth = -1;
		foreach ($iterator as $page)
		{
			$depth = $iterator->getDepth();
			$isActive = $page->isActive(true);
			if ($depth < $minDepth || !$this->accept($page))
			{
				// page is below minDepth or not accepted by acl/visibility
				continue;
			}
			elseif ($onlyActive && !$isActive)
			{
				// page is not active itself, but might be in the active branch
				$accept = false;
				if ($foundPage)
                {
					if ($foundPage->hasPage($page))
                    {
						// accept if page is a direct child of the active page
						$accept = true;
					}
					elseif ($foundPage->getParent()->hasPage($page))
					{
						// page is a sibling of the active page...
						if (!$foundPage->hasPages(!$this->renderInvisible)
							|| is_int($maxDepth) && $foundDepth + 1 > $maxDepth
						)
						{
							// accept if active page has no children, or the
							// children are too deep to be rendered
							$accept = true;
						}
					}
				}
				if (! $accept) continue;
			}

			// make sure indentation is correct
			$depth -= $minDepth;
			$divContainer = '';
			if ($depth > $prevDepth)
			{
				// start new ul tag
				if ($ulClass && $depth ==  0)
				{
					$ulClass = ' class="' . $escaper($ulClass) . '"';
					$divContainer = '<div class="ui container">';
				}
				else
				{
					$ulClass = ' class="menu"';
				}
				$html .= '<div' . $ulClass . '>' . $divContainer . PHP_EOL;
			}
			elseif ($prevDepth > $depth)
			{
				// close li/ul tags until we're at current depth
				for ($i = $prevDepth; $i > $depth; $i--)
                {
					$html .= '</div>' . PHP_EOL;
					$html .= '</div>' . PHP_EOL;
				}
				// close previous li tag
				$html .= '</div>' . PHP_EOL;
			}
			else
			{
				// close previous li tag
				$html .= '</div>' . PHP_EOL;
			}

			// render li tag and page
			$liClasses = [];

			// Is page active?
			if ($isActive)
			{
				$page->setClass($liActiveClass . ' ' . $page->getClass());
			}

			// Add CSS class from page to <li>
			if ($addClassToListItem && $page->getClass()) $liClasses[] = $page->getClass();

            // Add explicit CSS to <li>
            $semantic = $page->get('semantic');
            if (isset($semantic['liClass']) && !empty($semantic['liClass'])) $liClasses[] = $semantic['liClass'];

			$dropdown = false;
			if ($page->hasPages())
			{
				$liClasses[] = 'ui dropdown link item';
				$dropdown = true;
			}

			$liClass = empty($liClasses) ? '' : ' class="' . $escaper(implode(' ', $liClasses)) . '"';
			$html .= '<div' . $liClass . '>' . PHP_EOL
				. $this->htmlify($page, $escapeLabels, $addClassToListItem, $dropdown) . PHP_EOL;

			// store as previous depth for next iteration
			$prevDepth = $depth;
		}

		if ($html) {
			// done iterating container; close open ul/li tags
			for ($i = $prevDepth + 1; $i > 0; $i--)
			{
				$html .= '</div>' . PHP_EOL
					. '</div>' . PHP_EOL;
			}
			$html .= '</div>';
			$html = rtrim($html, PHP_EOL);
		}

		return $html;
	}

	/**
	 * Returns an HTML string containing an 'a' element for the given page if
	 * the page's href is not empty, and a 'span' element if it is empty.
	 *
	 * Overrides {@link AbstractHelper::htmlify()}.
	 *
	 * @param  AbstractPage $page               page to generate HTML for
	 * @param  bool         $escapeLabel        Whether or not to escape the label
	 * @param  bool         $addClassToListItem Whether or not to add the page class to the list item
	 * @return string
	 */
	public function htmlify(AbstractPage $page, $escapeLabel = true, $addClassToListItem = false, $dropdown = false)
	{
		// get attribs for element
		$attribs = [
			'id'     => $page->getId(),
			'title'  => $this->translate($page->getTitle(), $page->getTextDomain())
		];

		$semantic = $page->get('semantic');

		if ($addClassToListItem === false)
		{
			$attribs['class'] = $page->getClass();
		}

		$label = $this->translate($page->getLabel(), $page->getTextDomain());
		if ($escapeLabel === true)
		{
			/** @var \Zend\View\Helper\EscapeHtml $escaper */
			$escaper = $this->view->plugin('escapeHtml');
			$label = $escaper($label);
		}

        //-- Se agrega un icono a la etiqueta del menú
		if (isset($semantic['icon']) && $semantic['icon'])
		{
			$label = '<i class="' . $semantic['icon'] . '"></i>' . $label;
		}

        //-- Se agregan atributos adicionales al elemento
        if (isset($semantic['attributes']) && $semantic['attributes'])
        {
            $attribs = array_merge($attribs, $semantic['attributes']);
        }

		// does page have a href?
		$href = $page->getHref();
		if (! $href)
		{
			$element = 'span';

			$attribs['class'] = str_replace('active', '', $attribs['class']);
		}
		else if ($href && !$dropdown)
		{
			$element = 'a';
			$attribs['href'] = $href;
			$attribs['target'] = $page->getTarget();
		}
		else
		{
			$element = 'span';
			$attribs['class'] = $page->getClass();
		}

		$html  = '<' . $element . $this->htmlAttribs($attribs) . '>';
		$html .= $label;
		$html .= '</' . $element . '>';

		if ($dropdown) $html .= '<i class="dropdown icon"></i>';

		return $html;
	}
}
