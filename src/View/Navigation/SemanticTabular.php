<?php

/**
 * Semantic UI Utils
 *
 * @link      https://idmarinas@bitbucket.org/idmarinas/semanticutils for the canonical source repository
 * @copyright Copyright (c) 2017 Iván Diaz Marinas, IDMarinas. (http://www.infommo.es)
 * @license   See License.md
 */

namespace Idmarinas\SemanticUi\View\Navigation;

class SemanticTabular extends SemanticMenu
{
	/**
	 * CSS class for style menu
	 * @url http://semantic-ui.com/collections/menu.html
	 *
	 * @var string
	 */
	protected $ulClass = 'ui secondary pointing menu';
}
